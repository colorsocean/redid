package redid

import (
	"encoding/json"
	"fmt"

	"github.com/garyburd/redigo/redis"
)

type ObtainFn func(pref string, conn redis.Conn, meta interface{}) (int, error)

func Obtain(pref string, conn redis.Conn, meta interface{}) (int, error) {
	return (&ID{P: pref, RC: conn}).Obtain(meta)
}

type ID struct {
	P  string
	RC redis.Conn
	//RP *redis.Pool
}

func (this *ID) keyCtr() string {
	return this.P
}

func (this *ID) keyData() string {
	return fmt.Sprint(this.P, "+")
}

func (this *ID) Obtain(meta interface{}) (int, error) {
	id, err := redis.Int(this.RC.Do("incr", this.keyCtr()))
	if err != nil {
		return 0, err
	}
	if meta != nil {
		err = this.SetMeta(id, meta)
		if err != nil {
			return 0, err
		}
	}
	return id, nil
}

func (this *ID) SetMeta(id int, meta interface{}) error {
	data, err := marshal(meta)
	if err != nil {
		return err
	}
	_, err = this.RC.Do("hmset", this.keyData(), id, data)
	return err
}

func (this *ID) GetMeta(id int, metaOut interface{}) error {
	v, err := this.RC.Do("hmget", this.keyData(), id)
	if err != nil {
		return err
	}
	if v, ok := v.([]interface{}); ok && len(v) == 1 && v[0] == nil {
		return nil
	}
	data, err := redis.Bytes(v.([]interface{})[0], err)
	if err != nil {
		return err
	}
	return unmarshal(data, metaOut)
}

func marshal(v interface{}) ([]byte, error) {
	return json.Marshal(v)
}

func unmarshal(data []byte, vOut interface{}) error {
	return json.Unmarshal(data, vOut)
}
