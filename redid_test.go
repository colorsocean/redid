package redid

import (
	"testing"

	"github.com/garyburd/redigo/redis"
)

func perror(err error) {
	if err != nil {
		panic(err)
	}
}

type Meta struct {
	Type string
}

func BenchmarkObtain(b *testing.B) {
	b.ReportAllocs()
	conn, err := redis.Dial("tcp", "localhost:6379")
	perror(err)
	defer conn.Close()
	rid := ID{"uid", conn}

	b.ResetTimer()
	b.StartTimer()
	for i := 0; i < b.N; i++ {
		_, err := rid.Obtain(nil)
		perror(err)
	}
	b.StopTimer()
}

func BenchmarkObtainRaw(b *testing.B) {
	b.ReportAllocs()
	conn, err := redis.Dial("tcp", "localhost:6379")
	perror(err)
	defer conn.Close()
	rid := ID{"uid", conn}

	b.ResetTimer()
	b.StartTimer()
	for i := 0; i < b.N; i++ {
		rid.RC.Do("incr", "sdfsda")
	}
	b.StopTimer()
}

func TestAll(t *testing.T) {

	conn, err := redis.Dial("tcp", "localhost:6379")
	perror(err)
	defer conn.Close()

	rid := ID{"uid", conn}
	id, err := rid.Obtain(nil)
	perror(err)
	t.Log("ID: ", id)

	var m Meta
	perror(rid.GetMeta(0, &m))
	t.Log("Meta: ", m)

	id, err = rid.Obtain(Meta{"HELLO, REDIS>"})
	perror(err)
	t.Log("ID: ", id)

	perror(rid.GetMeta(id, &m))
	t.Log("Meta: ", m)
}
